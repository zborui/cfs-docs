---
title: 修箱批复
icon: tree
order: 2
---

# 修箱批复

### 菜单： 集装箱管理- - ->集装箱查询
### 功能： 修理批复

1. 选择在场集装箱
2. 单击*_修箱报价批复_*按钮
3. ![](/assets/image/fixreply.png)
4. *_批复类型_*选择批复或者退回
5. 录入批复内容
6. 单击*_保存_*按钮，箱属性中的修理状态变为‘已批复’或‘退回’
