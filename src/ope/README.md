---
title: 箱动态流程
index: false
icon: laptop-code
order: 1
---


一个箱子的完整管理范围，是从箱进场开始到箱出场截止,期间可能会包含多个动态，动态变动严格按照下图进行。
```mermaid
flowchart LR
    inyard((在场箱))
    箱进场 --> inyard
    style inyard fill:#f9f,stroke:#333,stroke-width:2px
    inyard --> 箱出场
    inyard --> 起租出场
    起租出场 --> 退租进场
    退租进场 --> inyard
    inyard --> 站间调箱出场
    站间调箱出场 --> 站间调箱进场
    站间调箱进场 --> inyard

```

# 基本操作步骤
1. 创建计划: 为每一个要进行的箱动态创建计划。
2. 计划预定箱号: 为计划指定允许操作的箱号。
3. 箱进出场: 场站按照计划操作箱进出场，产生箱动态。
<!-- <Catalog /> -->

