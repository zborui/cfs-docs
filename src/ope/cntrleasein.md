---
title: 退租进场流程
icon: gears
order: 3
---

# 箱退租进场

1. 创建计划： 集装箱管理 - - -> 进场计划
    - ![](/assets/image/menu_inplan.png)
    - 鼠标单击 *_增加_* 按钮
    - 在弹出的进场计划维护窗口中， *_作业目的_* 选择 *_退租进场_*
    - 输入其他有红色标志的字段
    - 如果有计划关联费用，鼠标单击  *_增加费用_* 按钮， 此处输入的费用，在箱进场的时候，按箱生成。
    - ![](/assets/image/planfee.png)    
        1. 输入费用信息， 点击费用行右侧的 *_确定_* 按钮.
        2. 有多条费用的，继续点击 *_增加费用_* 按钮
    - 鼠标单击 *_保存_* 按钮， 完成计划创建.
2. 计划预定箱号： 必须是已出租出场的箱子
    - 选中创建的计划。
    - 鼠标单击 *_预定箱号_* 按钮
    - ![](/assets/image/selectplan.png)
    - 在弹出的计划预定箱号窗口中，鼠标单击 *_增加_* 按钮
    - 在弹出的预定箱号窗口的文本框中，输入箱号，可粘贴多个箱号
    - ![](/assets/image/precntrs.png)
    - 单击 *_保存_* 按钮,  完成箱号预定。
3. 箱进场：堆场 - - -> 箱进场
    - ![](/assets/image/menu_cntrin.png)
    - 界面打开后，会显示当前站点未完成的所有进场计划
    - ![](/assets/image/cntrin.png)
    - 鼠标单击要进场箱所属的计划， 计划信息会同步到箱子上
    - 有未进场预定箱号的，在箱号处下拉选择箱号，不能手工录入。
    - 录入进场时间，不能跨天
    - 单击 *_进场_* 按钮

