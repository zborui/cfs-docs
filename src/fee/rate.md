---
title: 汇率维护
icon: cloud
order: 2
---

# 汇率维护

### 菜单： 商务管理- - ->汇率维护
### 功能： 维护两种货币间汇率， 在两种货币间核销费用时使用
![](/assets/image/rate.png)
上图中的第一行表示： 实收付欧元核销应收付港币的时候，1欧元核销7.77港币。如果实收付港币核销应收付欧元，则需要再添加一条港币转欧元。