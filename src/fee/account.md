---
title: 资金账号管理
icon: cloud
order: 3
---

# 资金账号管理

### 菜单： 商务管理- - ->资金账号管理
### 功能： 维护公司资金账号
![](/assets/image/account.png)
