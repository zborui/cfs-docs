---
title: 经营图表
icon: bolt
order: 9
---

# 经营图表

### 菜单： 商务管理- - ->经营图表
### 功能： 以图表形式展示业务费用，货币单位人民币
1. 选择开始月份、截止月份，成本中心可选
2. 图1按月份显示应收、应付、利润。
3. 图2以一级费目分组显示应收
4. 图3以一级费目分组显示应付
5. ![](/assets/image/businessechart.png)
