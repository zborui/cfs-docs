---
title: 货币维护
icon: hippo
order: 1
---

# 货币维护

### 菜单： 商务管理- - ->货币维护
### 功能： 维护货币种类，*_人民币汇率_* 在需要转换成人民币金额显示的报表中使用
![](/assets/image/currency.png)