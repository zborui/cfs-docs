import { sidebar } from "vuepress-theme-hope";

export default sidebar({
    "/": [
        // "",
        // "portfolio",
        {
            text: "操作流程",
            icon: "laptop-code",
            prefix: "ope/",
            link: "ope/",
            children: "structure",
        },
        {
            text: "修箱",
            icon: "hand",
            prefix: "repair/",
            link: "repair/",
            children: "structure",
        },
        {
            text: "商务",
            icon: "book",
            prefix: "fee/",
            link: "fee/",
            children: "structure",
        },
        {
            text: "核销",
            icon: "fire",
            prefix: "audit/",
            link: "audit/",
            children: "structure",
        },
    ],
});
