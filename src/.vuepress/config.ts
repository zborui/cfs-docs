import { defineUserConfig } from "vuepress";
import { hopeTheme } from "vuepress-theme-hope";
import theme from "./theme.js";

export default defineUserConfig({
    base: "/docs/",

    lang: "zh-CN",
    title: "操作手册",
    description: "集装箱租售系统操作手册",

    theme,
    

    // 和 PWA 一起启用
    // shouldPrefetch: false,
});
